import json
import os
import boto3
from uuid import uuid4

s3 = boto3.client("s3")

def get_s3_url(event, context):
    event["body"] = json.loads(event["body"])
    return {
        "body": s3.generate_presigned_url(
            ClientMethod="put_object",
            Params={
                "Bucket": os.environ["BUCKET"],
                "Key": str(uuid4()),
                "ContentType": event["body"]["type"],
                "ACL": "public-read",
            },
        )
    }
