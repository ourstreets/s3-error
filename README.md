# S3 presigned URL upload error

This repo is a minimal reproduction of the error we're experiencing using presigned S3 upload URLs
in us-east-2

## SOLUTION
Was missing the `x-amz-acl: public-read` header. Adding it fixes the problem.

## How to reproduce
1. Install serverless: `npm i -g serverless`
1. Add some random characters to the bucket name in `serverless.yml`
1. Deploy `sls deploy`, note the API URL in the output for the step below
1. Generate an upload URL `URL=$(curl -s -X POST https://xxxxxxxxxx.execute-api.us-east-2.amazonaws.com/dev/ --data-binary '{"type":"image/jpeg"}')`
1. Upload an image `curl -L -v -X PUT "$URL" -T '/path/to/an/image.jpg' -H 'Content-Type: image/jpeg'`

## Falsifiability
This works just fine in us-east-1. Repeat the above process with `sls deploy -r us-east-1` instead

## Attempts to fix
I've attempted to no avail to fix by specifying the signature version to use by creating the s3 client like
this:
```python
s3 = boto3.client("s3", config=Config(signature_version="s3v4"))
```

## Output
```
[:s3-error] master $ sls deploy
Serverless: Packaging service...
Serverless: Uploading CloudFormation file to S3...
Serverless: Uploading artifacts...
Serverless: Uploading service s3-error.zip file to S3 (7.33 KB)...
Serverless: Validating template...
Serverless: Updating Stack...
Serverless: Checking Stack update progress...
..............
Serverless: Stack update finished...
Service Information
service: s3-error
stage: dev
region: us-east-2
stack: s3-error-dev
resources: 11
api keys:
  None
endpoints:
  POST - https://6kvyqfa5qd.execute-api.us-east-2.amazonaws.com/dev/
functions:
  getS3Url: s3-error-dev-getS3Url
layers:
  None
Serverless: Run the "serverless" command to setup monitoring, troubleshooting and testing.
[:s3-error] master $ URL=$(curl -s -X POST https://6kvyqfa5qd.execute-api.us-east-2.amazonaws.com/dev/ --data-binary '{"type":"image/jpeg"}')
[:s3-error] master $ curl -L -v -X PUT "$URL" -T '/home/dschep/Downloads/20191218_164145.jpg' -H 'Content-Type: image/jpeg'
*   Trying 52.216.184.35...
* TCP_NODELAY set
* Connected to test-s3-presign-error-bucket-asfaf-us-east-2.s3.amazonaws.com (52.216.184.35) port 443 (#0)
* ALPN, offering h2
* ALPN, offering http/1.1
* successfully set certificate verify locations:
*   CAfile: /etc/ssl/certs/ca-certificates.crt
  CApath: /etc/ssl/certs
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
* TLSv1.3 (IN), TLS handshake, Server hello (2):
* TLSv1.2 (IN), TLS handshake, Certificate (11):
* TLSv1.2 (IN), TLS handshake, Server key exchange (12):
* TLSv1.2 (IN), TLS handshake, Server finished (14):
* TLSv1.2 (OUT), TLS handshake, Client key exchange (16):
* TLSv1.2 (OUT), TLS change cipher, Client hello (1):
* TLSv1.2 (OUT), TLS handshake, Finished (20):
* TLSv1.2 (IN), TLS handshake, Finished (20):
* SSL connection using TLSv1.2 / ECDHE-RSA-AES128-GCM-SHA256
* ALPN, server did not agree to a protocol
* Server certificate:
*  subject: C=US; ST=Washington; L=Seattle; O=Amazon.com, Inc.; CN=*.s3.amazonaws.com
*  start date: Nov  9 00:00:00 2019 GMT
*  expire date: Mar 12 12:00:00 2021 GMT
*  subjectAltName: host "test-s3-presign-error-bucket-asfaf-us-east-2.s3.amazonaws.com" matched cert's "*.s3.amazonaws.com"
*  issuer: C=US; O=DigiCert Inc; OU=www.digicert.com; CN=DigiCert Baltimore CA-2 G2
*  SSL certificate verify ok.
> PUT /4a1157a5-6f3b-498d-b3c3-5a9f22b9e125?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIAQUS6AKYIZAAQFHUS%2F20191230%2Fus-east-2%2Fs3%2Faws4_req
uest&X-Amz-Date=20191230T225209Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=content-type%3Bhost%3Bx-amz-acl&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEHcaCXVzLWVh
c3QtMiJHMEUCIQDzOADp8vB1i50z9z3Pz1jM8Oq5ucTIM9Vn70veNZXivwIgfBnssIHONkuqGK3y0gzQNTNdtcXIi1qbtcgLkTCxGQcq3QEI4P%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgwwNDQyM
jA1NjkxMDUiDGBBBsmphkT3aH%2FG%2ByqxAV1e%2ByhsErsfPFvUUrXT4wULnbE%2ByZ7W5g0TyFSA0FkqQKLgRWlGwFPZ4PYovrYSiWAzPlUuRZ2rTArwL9ILwlb4Zu%2FcWu1xOYYN2ecqk4JKsJQA
KUa8QWOLhKNoVVU1RxWswq7KCnVAAYV9czT%2FNmtQQtixgP7j8PsDog6R3%2F5wgDFn1T78seLDSxcI9HuuC6xL%2FYa87kh%2BxkvFGav%2FbWY4ZWjveXdshhAkdJ9PE4HRtzCZ%2FKnwBTrgAcm6k
lfi65fh%2FOoist2PoGrLYBQcjgD1uQgz7F19HM81tXuhzoOVcfTpuYxOzZO%2FL4tMptTIndyFwGofJ7GdlIpSLN%2F9SBsuSkn8RPrWOAqm3bgNqHUm4fCU27NegDWHb8cQwGj%2FCHAB4tsWuUc8B6
AvcdWb4ZdMpalzJyPSqK965TsJTRHEhRyEVHbInOCAaZE2eRw9n8eagN839ptcL1%2F7gOZyfDvqqQmZiCwPLHIsrJZaPe%2Bivpptqt8hFVmmGd8OLzZqZ3lvoWyoYOLeRf8FTIomNrtyPsnHBJnUxPZ
X&X-Amz-Signature=972a007997665e1c4167ad2c27dd3f5c8deaf120d7f53905dbabb6b51323d094 HTTP/1.1
> Host: test-s3-presign-error-bucket-asfaf-us-east-2.s3.amazonaws.com
> User-Agent: curl/7.58.0
> Accept: */*
> Content-Type: image/jpeg
> Content-Length: 2916025
> Expect: 100-continue
>
< HTTP/1.1 307 Temporary Redirect
< x-amz-bucket-region: us-east-2
< x-amz-request-id: 799E955CAE0FEB07
< x-amz-id-2: d/mAroT0WMEche0wCJ9LGiXi0G63dfAcCv8c5/vok12/ClnIxuNoYi0rBHP1k20yCXN7uD0pzGw=
< Location: https://test-s3-presign-error-bucket-asfaf-us-east-2.s3.us-east-2.amazonaws.com/4a1157a5-6f3b-498d-b3c3-5a9f22b9e125?X-Amz-Algorithm=AWS4-HMA
C-SHA256&X-Amz-Credential=ASIAQUS6AKYIZAAQFHUS%2F20191230%2Fus-east-2%2Fs3%2Faws4_request&X-Amz-Date=20191230T225209Z&X-Amz-Expires=3600&X-Amz-SignedHead
ers=content-type%3Bhost%3Bx-amz-acl&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEHcaCXVzLWVhc3QtMiJHMEUCIQDzOADp8vB1i50z9z3Pz1jM8Oq5ucTIM9Vn70veNZXivwIgfBnssIHO
NkuqGK3y0gzQNTNdtcXIi1qbtcgLkTCxGQcq3QEI4P%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgwwNDQyMjA1NjkxMDUiDGBBBsmphkT3aH%2FG%2ByqxAV1e%2ByhsErsfPFvUUrXT4wULnbE%2By
Z7W5g0TyFSA0FkqQKLgRWlGwFPZ4PYovrYSiWAzPlUuRZ2rTArwL9ILwlb4Zu%2FcWu1xOYYN2ecqk4JKsJQAKUa8QWOLhKNoVVU1RxWswq7KCnVAAYV9czT%2FNmtQQtixgP7j8PsDog6R3%2F5wgDFn
1T78seLDSxcI9HuuC6xL%2FYa87kh%2BxkvFGav%2FbWY4ZWjveXdshhAkdJ9PE4HRtzCZ%2FKnwBTrgAcm6klfi65fh%2FOoist2PoGrLYBQcjgD1uQgz7F19HM81tXuhzoOVcfTpuYxOzZO%2FL4tMp
tTIndyFwGofJ7GdlIpSLN%2F9SBsuSkn8RPrWOAqm3bgNqHUm4fCU27NegDWHb8cQwGj%2FCHAB4tsWuUc8B6AvcdWb4ZdMpalzJyPSqK965TsJTRHEhRyEVHbInOCAaZE2eRw9n8eagN839ptcL1%2F7
gOZyfDvqqQmZiCwPLHIsrJZaPe%2Bivpptqt8hFVmmGd8OLzZqZ3lvoWyoYOLeRf8FTIomNrtyPsnHBJnUxPZX&X-Amz-Signature=972a007997665e1c4167ad2c27dd3f5c8deaf120d7f53905db
abb6b51323d094
< Content-Type: application/xml
< Transfer-Encoding: chunked
< Date: Mon, 30 Dec 2019 22:52:14 GMT
< Connection: close
< Server: AmazonS3
<
* Closing connection 0
* TLSv1.2 (OUT), TLS alert, Client hello (1):
* Issue another request to this URL: 'https://test-s3-presign-error-bucket-asfaf-us-east-2.s3.us-east-2.amazonaws.com/4a1157a5-6f3b-498d-b3c3-5a9f22b9e12
5?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIAQUS6AKYIZAAQFHUS%2F20191230%2Fus-east-2%2Fs3%2Faws4_request&X-Amz-Date=20191230T225209Z&X-Amz-Exp
ires=3600&X-Amz-SignedHeaders=content-type%3Bhost%3Bx-amz-acl&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEHcaCXVzLWVhc3QtMiJHMEUCIQDzOADp8vB1i50z9z3Pz1jM8Oq5uc
TIM9Vn70veNZXivwIgfBnssIHONkuqGK3y0gzQNTNdtcXIi1qbtcgLkTCxGQcq3QEI4P%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgwwNDQyMjA1NjkxMDUiDGBBBsmphkT3aH%2FG%2ByqxAV1e%2B
yhsErsfPFvUUrXT4wULnbE%2ByZ7W5g0TyFSA0FkqQKLgRWlGwFPZ4PYovrYSiWAzPlUuRZ2rTArwL9ILwlb4Zu%2FcWu1xOYYN2ecqk4JKsJQAKUa8QWOLhKNoVVU1RxWswq7KCnVAAYV9czT%2FNmtQ
QtixgP7j8PsDog6R3%2F5wgDFn1T78seLDSxcI9HuuC6xL%2FYa87kh%2BxkvFGav%2FbWY4ZWjveXdshhAkdJ9PE4HRtzCZ%2FKnwBTrgAcm6klfi65fh%2FOoist2PoGrLYBQcjgD1uQgz7F19HM81t
XuhzoOVcfTpuYxOzZO%2FL4tMptTIndyFwGofJ7GdlIpSLN%2F9SBsuSkn8RPrWOAqm3bgNqHUm4fCU27NegDWHb8cQwGj%2FCHAB4tsWuUc8B6AvcdWb4ZdMpalzJyPSqK965TsJTRHEhRyEVHbInOCA
aZE2eRw9n8eagN839ptcL1%2F7gOZyfDvqqQmZiCwPLHIsrJZaPe%2Bivpptqt8hFVmmGd8OLzZqZ3lvoWyoYOLeRf8FTIomNrtyPsnHBJnUxPZX&X-Amz-Signature=972a007997665e1c4167ad2c
27dd3f5c8deaf120d7f53905dbabb6b51323d094'
*   Trying 52.219.100.128...
* TCP_NODELAY set
* Connected to test-s3-presign-error-bucket-asfaf-us-east-2.s3.us-east-2.amazonaws.com (52.219.100.128) port 443 (#1)
* ALPN, offering h2
* ALPN, offering http/1.1
* successfully set certificate verify locations:
*   CAfile: /etc/ssl/certs/ca-certificates.crt
  CApath: /etc/ssl/certs
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
* TLSv1.3 (IN), TLS handshake, Server hello (2):
* TLSv1.2 (IN), TLS handshake, Certificate (11):
* TLSv1.2 (IN), TLS handshake, Server key exchange (12):
* TLSv1.2 (IN), TLS handshake, Server finished (14):
* TLSv1.2 (OUT), TLS handshake, Client key exchange (16):
* TLSv1.2 (OUT), TLS change cipher, Client hello (1):
* TLSv1.2 (OUT), TLS handshake, Finished (20):
* TLSv1.2 (IN), TLS handshake, Finished (20):
* SSL connection using TLSv1.2 / ECDHE-RSA-AES128-GCM-SHA256
* ALPN, server did not agree to a protocol
* Server certificate:
*  subject: C=US; ST=Washington; L=Seattle; O=Amazon.com, Inc.; CN=*.s3.us-east-2.amazonaws.com
*  start date: Nov  9 00:00:00 2019 GMT
*  expire date: Apr 22 12:00:00 2020 GMT
*  subjectAltName: host "test-s3-presign-error-bucket-asfaf-us-east-2.s3.us-east-2.amazonaws.com" matched cert's "*.s3.us-east-2.amazonaws.com"
*  issuer: C=US; O=DigiCert Inc; OU=www.digicert.com; CN=DigiCert Baltimore CA-2 G2
*  SSL certificate verify ok.
> PUT /4a1157a5-6f3b-498d-b3c3-5a9f22b9e125?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIAQUS6AKYIZAAQFHUS%2F20191230%2Fus-east-2%2Fs3%2Faws4_req
uest&X-Amz-Date=20191230T225209Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=content-type%3Bhost%3Bx-amz-acl&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEHcaCXVzLWVh
c3QtMiJHMEUCIQDzOADp8vB1i50z9z3Pz1jM8Oq5ucTIM9Vn70veNZXivwIgfBnssIHONkuqGK3y0gzQNTNdtcXIi1qbtcgLkTCxGQcq3QEI4P%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgwwNDQyM
jA1NjkxMDUiDGBBBsmphkT3aH%2FG%2ByqxAV1e%2ByhsErsfPFvUUrXT4wULnbE%2ByZ7W5g0TyFSA0FkqQKLgRWlGwFPZ4PYovrYSiWAzPlUuRZ2rTArwL9ILwlb4Zu%2FcWu1xOYYN2ecqk4JKsJQA
KUa8QWOLhKNoVVU1RxWswq7KCnVAAYV9czT%2FNmtQQtixgP7j8PsDog6R3%2F5wgDFn1T78seLDSxcI9HuuC6xL%2FYa87kh%2BxkvFGav%2FbWY4ZWjveXdshhAkdJ9PE4HRtzCZ%2FKnwBTrgAcm6k
lfi65fh%2FOoist2PoGrLYBQcjgD1uQgz7F19HM81tXuhzoOVcfTpuYxOzZO%2FL4tMptTIndyFwGofJ7GdlIpSLN%2F9SBsuSkn8RPrWOAqm3bgNqHUm4fCU27NegDWHb8cQwGj%2FCHAB4tsWuUc8B6
AvcdWb4ZdMpalzJyPSqK965TsJTRHEhRyEVHbInOCAaZE2eRw9n8eagN839ptcL1%2F7gOZyfDvqqQmZiCwPLHIsrJZaPe%2Bivpptqt8hFVmmGd8OLzZqZ3lvoWyoYOLeRf8FTIomNrtyPsnHBJnUxPZ
X&X-Amz-Signature=972a007997665e1c4167ad2c27dd3f5c8deaf120d7f53905dbabb6b51323d094 HTTP/1.1
> Host: test-s3-presign-error-bucket-asfaf-us-east-2.s3.us-east-2.amazonaws.com
> User-Agent: curl/7.58.0
> Accept: */*
> Content-Type: image/jpeg
> Content-Length: 2916025
> Expect: 100-continue
>
< HTTP/1.1 403 Forbidden
< x-amz-request-id: 302052F5212BEC21
< x-amz-id-2: lOatYPYdfu09B3kRYtlxGGxErzNf/KiMYhzpju6tIvTo1jKpIa59PwcOawAOEi9xR+98G7N37rU=
< Content-Type: application/xml
< Transfer-Encoding: chunked
< Date: Mon, 30 Dec 2019 22:52:14 GMT
< Connection: close
< Server: AmazonS3
<
<?xml version="1.0" encoding="UTF-8"?>
<Error><Code>SignatureDoesNotMatch</Code><Message>The request signature we calculated does not match the signature you provided. Check your key and signi
ng method.</Message><AWSAccessKeyId>ASIAQUS6AKYIZAAQFHUS</AWSAccessKeyId><StringToSign>AWS4-HMAC-SHA256
20191230T225209Z
20191230/us-east-2/s3/aws4_request
3122cb3708a1cee4987961306a5bf0491ebe7f9b5f73dd31e5a4853dbfc05c60</StringToSign><SignatureProvided>972a007997665e1c4167ad2c27dd3f5c8deaf120d7f53905dbabb6b
51323d094</SignatureProvided><StringToSignBytes>41 57 53 34 2d 48 4d 41 43 2d 53 48 41 32 35 36 0a 32 30 31 39 31 32 33 30 54 32 32 35 32 30 39 5a 0a 32
30 31 39 31 32 33 30 2f 75 73 2d 65 61 73 74 2d 32 2f 73 33 2f 61 77 73 34 5f 72 65 71 75 65 73 74 0a 33 31 32 32 63 62 33 37 30 38 61 31 63 65 65 34 39
38 37 39 36 31 33 30 36 61 35 62 66 30 34 39 31 65 62 65 37 66 39 62 35 66 37 33 64 64 33 31 65 35 61 34 38 35 33 64 62 66 63 30 35 63 36 30</StringToSig
nBytes><CanonicalRequest>PUT
/4a1157a5-6f3b-498d-b3c3-5a9f22b9e125
X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=ASIAQUS6AKYIZAAQFHUS%2F20191230%2Fus-east-2%2Fs3%2Faws4_request&amp;X-Amz-Date=20191230T225209Z&amp
;X-Amz-Expires=3600&amp;X-Amz-Security-Token=IQoJb3JpZ2luX2VjEHcaCXVzLWVhc3QtMiJHMEUCIQDzOADp8vB1i50z9z3Pz1jM8Oq5ucTIM9Vn70veNZXivwIgfBnssIHONkuqGK3y0gzQ
NTNdtcXIi1qbtcgLkTCxGQcq3QEI4P%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARAAGgwwNDQyMjA1NjkxMDUiDGBBBsmphkT3aH%2FG%2ByqxAV1e%2ByhsErsfPFvUUrXT4wULnbE%2ByZ7W5g0TyFSA0
FkqQKLgRWlGwFPZ4PYovrYSiWAzPlUuRZ2rTArwL9ILwlb4Zu%2FcWu1xOYYN2ecqk4JKsJQAKUa8QWOLhKNoVVU1RxWswq7KCnVAAYV9czT%2FNmtQQtixgP7j8PsDog6R3%2F5wgDFn1T78seLDSxcI
9HuuC6xL%2FYa87kh%2BxkvFGav%2FbWY4ZWjveXdshhAkdJ9PE4HRtzCZ%2FKnwBTrgAcm6klfi65fh%2FOoist2PoGrLYBQcjgD1uQgz7F19HM81tXuhzoOVcfTpuYxOzZO%2FL4tMptTIndyFwGofJ
7GdlIpSLN%2F9SBsuSkn8RPrWOAqm3bgNqHUm4fCU27NegDWHb8cQwGj%2FCHAB4tsWuUc8B6AvcdWb4ZdMpalzJyPSqK965TsJTRHEhRyEVHbInOCAaZE2eRw9n8eagN839ptcL1%2F7gOZyfDvqqQmZ
iCwPLHIsrJZaPe%2Bivpptqt8hFVmmGd8OLzZqZ3lvoWyoYOLeRf8FTIomNrtyPsnHBJnUxPZX&amp;X-Amz-SignedHeaders=content-type%3Bhost%3Bx-amz-acl
content-type:image/jpeg
host:test-s3-presign-error-bucket-asfaf-us-east-2.s3.us-east-2.amazonaws.com
x-amz-acl:

content-type;host;x-amz-acl
UNSIGNED-PAYLOAD</CanonicalRequest><CanonicalRequestBytes>DELETED</CanonicalRequestBytes><RequestId>302052F5212BEC21</RequestId><HostId>lOatYPYdf
u09B3kRYtlxGGxErzNf/KiMYhzpju6tIvTo1jKpIa59PwcOawAOEi9xR+98G7N37rU=</HostId></Error>
```
